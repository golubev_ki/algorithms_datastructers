#include <iostream>
#include <gtest/gtest.h>
#include "dyn_arr/inc/dyn_arr.hpp"
#include <cmath>

TEST(constructor_test, default_constructor) {
    data_structs::dyn_array<int> array;
    ASSERT_TRUE(array.is_empty());
    ASSERT_EQ(array.length(), 0);
}

TEST(constructor_test, init_list_constructor) {
    data_structs::dyn_array<size_t> array{1, 2, 3, 4};
    ASSERT_FALSE(array.is_empty());
    ASSERT_EQ(array.length(), 4);
    for (size_t i = 0; i < 4; ++i) {
        ASSERT_EQ(array[i], 1 + i);
    }
}

TEST(constructor_test, copy_constructor) {
    data_structs::dyn_array<size_t> array{1, 2, 3, 4};
    data_structs::dyn_array<size_t> another(array);
    array[0] = 10;
    another[0] = 11;
    ASSERT_FALSE(array[0] == another[0]);
    ASSERT_TRUE(array.length() == 4);
    ASSERT_EQ(array[0], 10);
    for (size_t i = 0; i < 3; ++i) {
        ASSERT_EQ(array[i + 1], i + 2);
    }

    ASSERT_TRUE(another.length() == 4);
    ASSERT_EQ(another[0], 11);
    for (size_t i = 0; i < 3; ++i) {
        ASSERT_EQ(another[i + 1], i + 2);
    }
}

TEST(constructor_test, move_constructor) {
    data_structs::dyn_array<size_t> array{3, 2, 1, 0};
    data_structs::dyn_array<size_t> another(std::move(array));
    ASSERT_TRUE(another.length() == 4);
    for (size_t i = 0; i < 4; ++i) {
        ASSERT_EQ(another[i], 3 - i);
    }
    ASSERT_TRUE(array.is_empty());
}

TEST(constructor_test, iterator_constructor) {
    size_t exp_length = 100;
    data_structs::dyn_array<size_t> array;

    for (size_t i = 0; i < exp_length; ++i) {
        array.append(i);
    }

    data_structs::dyn_array<size_t> another(array.begin(), array.end());
}

TEST(method_test, resize) {
    data_structs::dyn_array<size_t> array;
    size_t exp_len = 100;
    size_t exp_size = int(std::pow(2, int(std::log2(exp_len)) + 1));
    for (size_t i = 0; i < exp_len; ++i) {
        array.append(i);
    }
    ASSERT_FALSE(array.is_empty());
    ASSERT_EQ(array.length(), exp_len);
    ASSERT_EQ(array.alloc_size(), exp_size);
    for (size_t i = 0; i < exp_len; ++i) {
        ASSERT_EQ(array[i], i);
    }
}

TEST(iterator_test, random_access) {
    data_structs::dyn_array<size_t> array;
    size_t exp_len = 100;
    size_t exp_size = int(std::pow(2, int(std::log2(exp_len)) + 0.5));
    for (size_t i = 0; i < exp_len; ++i) {
        array.append(i);
    }
    size_t idx = 0;
    for (const auto &x: array) {
        ASSERT_EQ(x, idx);
        idx++;
    }
    idx = 0;

    for (auto it = array.begin(); it != array.end(); ++it) {
        ASSERT_EQ(*it, idx);
        idx++;
    }

    auto it = array.begin();
    for (idx = 0; idx < exp_len; ++idx) {
        ASSERT_EQ(it[idx], idx);
    }

    it = array.end() - 1;
    idx = exp_len - 1;
    for (; it != array.begin() - 1; --it) {
        ASSERT_EQ(*it, idx);
        --idx;
    }

    auto e_it = array.end();
    auto b_it = array.begin();
    ASSERT_EQ(e_it - b_it, array.length());
    ASSERT_TRUE(b_it < e_it);
    ASSERT_TRUE(e_it > b_it);
}

TEST(weird_types_test, non_copyable) {
    struct non_copyable_type {
        int payload;

        non_copyable_type() = default;

        ~non_copyable_type() = default;

        non_copyable_type(non_copyable_type &&) = default;

        explicit non_copyable_type(int val) : payload(val) {}

        non_copyable_type(const non_copyable_type &) = delete;


    };
    static_assert(not std::is_copy_constructible<non_copyable_type>::value);

    data_structs::dyn_array<non_copyable_type> array;
    size_t exp_length = 1000;
    for (size_t i = 0; i < exp_length; ++i) {
        auto tmp = non_copyable_type(i);
        try {
            array.append(tmp);
        } catch (std::runtime_error &e) {
            array.append(std::move(tmp));
        }
    }
    for (size_t i = 0; i < exp_length; ++i) {
        ASSERT_EQ(array[i].payload, i);
    }
    while (not array.is_empty()) {
        auto tmp = array.pop();
        ASSERT_EQ((*tmp).payload, array.length());
    }
}

TEST(weird_types_test, non_moveable) {
    struct non_movable_type {
        int payload;

        non_movable_type() = default;

        ~non_movable_type() = default;

        non_movable_type(const non_movable_type &) = default;

        explicit non_movable_type(int val) : payload(val) {}

        non_movable_type(non_movable_type &&) = delete;

    };

    static_assert(not std::is_move_constructible<non_movable_type>::value);

    data_structs::dyn_array<non_movable_type> array;
    size_t exp_length = 1000;
    for (size_t i = 0; i < exp_length; ++i) {
        auto tmp = non_movable_type(i);
        array.append(tmp);
    }
    for (size_t i = 0; i < exp_length; ++i) {
        ASSERT_EQ(array[i].payload, i);
    }

    while (not array.is_empty()) {
        auto tmp = array.pop();
        ASSERT_EQ((*tmp).payload, array.length());
    }
}

TEST(method_test, pop) {
    data_structs::dyn_array<size_t> array;
    size_t exp_len = 100;
    size_t exp_size = int(std::pow(2, int(std::log2(exp_len)) + 1));
    for (size_t i = 0; i < exp_len; ++i) {
        array.append(i);
    }
    size_t counter = 0;
    while (not array.is_empty()) {
        auto tmp = array.pop();
        ASSERT_EQ(*tmp, array.length());
        counter++;
    }
}

TEST(border_conditions, empty_array) {
    data_structs::dyn_array<size_t> array;
    auto tmp = array.pop();
    ASSERT_EQ(tmp.release(), nullptr);
}

TEST(iterator_test, const_iterator) {
    size_t exp_len = 1000;
    data_structs::dyn_array<size_t> array;
    for (size_t i = 0; i < exp_len; ++i) {
        array.append(i);
    }
    typename data_structs::dyn_array<size_t>::const_iterator it;
    it = array.begin();
    const size_t val = *it;
    ASSERT_EQ(val, array[0]);
    for (; it != array.end(); ++it) {
        auto value = not std::is_assignable<decltype(*it), decltype(array.end() - it)>::value;
        ASSERT_TRUE(value);
    }
}

TEST(method_test, emplace_append) {
    struct complex_constructor_type {
        int a, b, c;
        double d, e, f;
        std::string h;

        complex_constructor_type(int a, int b, int c, double d, double e, double f, const std::string &h) :
                a(a), b(b), c(c), d(d), e(e), f(f), h(h) {}

        bool operator==(const complex_constructor_type &other) const {
            if (a != other.a) {
                return false;
            }
            if (b != other.b) {
                return false;
            }
            if (c != other.c) {
                return false;
            }
            if (d != other.d) {
                return false;
            }
            if (e != other.e) {
                return false;
            }
            if (f != other.f) {
                return false;
            }
            if (h != other.h) {
                return false;
            }
            return true;
        }
    };
    auto val = complex_constructor_type(1, 2, 3, 1.23, 1.01, 141.0, "a;lskdfj;a");
    data_structs::dyn_array<complex_constructor_type> array;
    array.emplace_append(1, 2, 3, 1.23, 1.01, 141.0, "a;lskdfj;a");
    auto poped_val = array.pop();
    ASSERT_EQ(*poped_val, val);
}

TEST(method_test, comp_operator) {
    data_structs::dyn_array array{12, 4, 5, 6};
    data_structs::dyn_array another(array);
    ASSERT_TRUE(array == another);
    ASSERT_FALSE(array != another);
    another[0] = 2;
    ASSERT_FALSE(array == another);
    ASSERT_TRUE(array != another);
}

TEST(method_test, assign_operator) {
    data_structs::dyn_array<size_t> array{12, 4, 5, 6};
    data_structs::dyn_array<size_t> another;
    another = array;
    ASSERT_TRUE(another == array);
}

TEST(method_test, move_assign_operator) {
    data_structs::dyn_array<size_t> array{12, 4, 5, 6};
    data_structs::dyn_array<size_t> array_1{12, 4, 5, 6};
    data_structs::dyn_array<size_t> another;
    another = std::move(array);
    ASSERT_TRUE(another == array_1);
    ASSERT_TRUE(array.is_empty());

}


int main() {
//##checking for a memory leaks hard way##

//    size_t load = 1000;
//    while(true){
//        data_structs::dyn_array<size_t> array{1, 2, 3};
//        for(size_t i = 0; i < load; ++i){
//            array.append(i);
//        }
//    }

    ::testing::InitGoogleTest();
    return RUN_ALL_TESTS();
}