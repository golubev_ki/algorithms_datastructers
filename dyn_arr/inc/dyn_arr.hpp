//
// Created by Kirill on 2019-07-30.
//

#ifndef HAT_DYN_ARR_HPP
#define HAT_DYN_ARR_HPP

#include <cstddef>
#include <type_traits>
#include <stdexcept>
#include <memory>
#include <algorithm>
#include <iostream>
#include <vector>




//!  Class dynamic array with same functionality as std::vector
/*!
  This is a code for academic purposes. It's further usage in project is not recommended.
*/

namespace data_structs {

    template<typename T>
    class dyn_array {
    private:
        typedef std::allocator<T> alloc_type;
        typedef std::allocator_traits<alloc_type> builder;
        typedef std::iterator<std::random_access_iterator_tag, T> iter_supertype;
        alloc_type a;
        /*! This is help values which are storing information about allocated size of the array
         * */

        size_t size;

        /*! This is help values which are storing information about actual it's actual size
         * */

        size_t capacity;

        //! A storage pointer.
        /*! This is the payload. This pointer is used for storing objects of type T.*/
        T *storage;

        //! A class for checking if bool operator==(const T&) exists for implementing bool operator==(const dyn_array&)
        template<typename Type>
        class is_comparable {
            template<class, class=std::void_t<> >
            class value_type : public std::false_type {
            };

            template<class __Type>
            class value_type<__Type, std::void_t<decltype(std::declval<__Type>() == std::declval<__Type>())> >
                    : public std::true_type {
            };

        public:

            static const constexpr value_type<Type> value;
        };

        //! A class implementing const and non-const random access iterator
        template<bool is_const_it = true>
        class gen_iterator : public iter_supertype {
            typedef typename std::conditional<is_const_it, const T *, T *>::type pointer_type;
            typedef typename std::conditional<is_const_it, const T, T>::type return_value_type;
            pointer_type p;
        public:
            gen_iterator() noexcept {
                p = nullptr;
            }

            gen_iterator(T *x) noexcept : p(x) {}

            gen_iterator(const gen_iterator<true> &other) noexcept : p(other.p) {}

            gen_iterator(const gen_iterator<false> &other) noexcept  : p(other.p) {}

            gen_iterator &operator++() noexcept {
                ++p;
                return *this;
            }

            const gen_iterator operator++(int) noexcept {
                const gen_iterator tmp(*this);
                operator++();
                return tmp;
            }

            gen_iterator &operator--() noexcept {
                --p;
                return *this;
            }

            const gen_iterator operator--(int) noexcept {
                const gen_iterator tmp(*this);
                operator--();
                return tmp;
            }

            bool operator==(const gen_iterator &other) const noexcept {
                return p == other.p;
            }

            bool operator!=(const gen_iterator &other) const noexcept {
                return not operator==(other);
            }

            bool operator<=(const gen_iterator &other) const noexcept {
                return *this - other <= 0;
            }

            bool operator>=(const gen_iterator &other) const noexcept {
                return *this - other >= 0;
            }

            bool operator<(const gen_iterator &other) const noexcept {
                return not(*this >= other);
            }

            bool operator>(const gen_iterator &other) const noexcept {
                return not(*this <= other);
            }

            const T &operator*() const {
                return *p;
            }

            return_value_type &operator*() {
                return *p;
            }

            gen_iterator &operator+=(int n) noexcept {
                p += n;
                return *this;
            }

            gen_iterator &operator-=(int n) noexcept {
                p -= n;
                return *this;
            }

            gen_iterator operator+(int n) noexcept {
                return gen_iterator(*this) += n;
            }

            gen_iterator operator-(int n) noexcept {
                return gen_iterator(*this) -= n;
            }

            T &operator[](int n) {
                return *(*this + n);
            }

            const T &operator[](int n) const {
                return *(*this + n);
            }

            friend gen_iterator operator-(int shift, const gen_iterator &it) noexcept {
                return it - shift;
            }

            friend gen_iterator operator+(int shift, const gen_iterator &it) noexcept {
                return it + shift;
            }

            int operator-(const gen_iterator<true> &another) const {
                return p - another.p;
            }

            int operator-(const gen_iterator<false> &another) const {
                return p - another.p;
            }

            friend class gen_iterator<true>;

            friend class gen_iterator<false>;

        };

    public:

        //! Resize function.
        /*!
         * \details Resize function is called each time dynamic array is in lack of space. Throws when:
         * \li allocator::allocate throws \n
         * \li move constructor of T throws \n
         * \li if not move constructable - when copy constructor throws \n
         * \li T is not copy constructable and not move constructable
         * \details If an exception accrues strong exception safety is guaranteed.\n
         * \param new_size: \n
         * default = -1: - resizing is doubling current size\n
         * greater then -1: - resizing to new_size, discarding the rest of elements
         * \return function returns nothing
        */
        void resize(ssize_t new_size = -1) {
            size_t realloc_size = new_size;
            if (new_size == -1) {
                realloc_size = 2 * size;
            }
            if (size == 0) {
                storage = builder::allocate(a, 1);
                size = 1;
                return;
            }
            auto tmp = builder::allocate(a, realloc_size);
            if constexpr (std::is_move_constructible<T>::value) {//expecting move constructor to be noexcept
                for (size_t i = 0; i < std::min(capacity, realloc_size); ++i) {
                    builder::construct(a, &tmp[i], std::move(storage[i]));
                }
            } else if constexpr (std::is_copy_constructible<T>::value) {
                size_t new_capacity = 0;
                try {
                    for (; new_capacity < std::min(capacity, realloc_size); ++new_capacity) {
                        builder::construct(a, &tmp[new_capacity], storage[new_capacity]);
                    }
                } catch (const std::runtime_error &e) {
                    for (size_t i = 0; i < new_capacity; ++i) {
                        builder::destroy(a, &tmp[i]);
                    }
                    builder::deallocate(a, tmp, realloc_size);
                    throw;
                }
            } else {
                throw std::runtime_error("Can't resize array with non-copyable and non-movable elements");
            }
            for (size_t i = 0; i < capacity; ++i) {
                builder::destroy(a, &storage[i]);
            }
            builder::deallocate(a, storage, size);
            storage = tmp;
            size = realloc_size;
            capacity = std::min(capacity, realloc_size);
        }

        void append(const T &item) {
            if constexpr (not std::is_copy_constructible<T>::value) {
                throw (std::runtime_error("You can't append non-copyable element by reference"));
            } else {
                if (capacity + 1 >= size) {
                    resize();
                }
                builder::construct(a, &storage[capacity], item);
                ++capacity;
            }
        }

        void append(T &&item) noexcept(std::is_move_constructible<T>::value) {
            if constexpr (not std::is_move_constructible<T>::value) {
                throw (std::runtime_error("You can't append non-movable element by double reference"));
            } else {
                if (capacity + 1 >= size) {
                    resize();
                }
                builder::construct(a, &storage[capacity], std::move(item));
                ++capacity;
            }
        }

        void append(gen_iterator<true> begin, gen_iterator<true> end) {
            if constexpr (not std::is_copy_constructible<T>::value) {
                throw (std::runtime_error("You can't append non-copyable elements by passing iterators"));
            } else {
                size_t appended_size = end - begin;
                if (capacity + appended_size >= size) {
                    if (appended_size > size)
                        resize(capacity + appended_size);
                    else
                        resize();
                }
                auto soon_be_end = begin;
                try {
                    for (; soon_be_end != end; ++soon_be_end) {
                        builder::construct(a, &storage[capacity], *soon_be_end);
                        ++capacity;
                    }
                } catch (std::runtime_error &e) {
                    for (; soon_be_end != begin; --soon_be_end) {
                        --capacity;
                        builder::destroy(a, &storage[capacity]);
                    }
                    throw;
                }
            }

        }

        template<typename ...Args>
        void emplace_append(Args &&... args) {
            if (capacity + 1 >= size) {
                resize();
            }
            builder::construct(a, &storage[capacity], std::forward<Args>(args)...);
            ++capacity;
        }

        std::unique_ptr<T> pop() {
            if (capacity == 0) {
                return std::unique_ptr<T>(nullptr);
            }
            capacity--;
            T *tmp = builder::allocate(a, 1);
            if constexpr (std::is_move_constructible<T>::value) {
                builder::construct(a, tmp, std::move(storage[capacity]));
            } else if constexpr (std::is_copy_constructible<T>::value) {
                builder::construct(a, tmp, storage[capacity]);
            }
            builder::destroy(a, &storage[capacity]);
            return std::unique_ptr<T>(tmp);
        }

        size_t length() const noexcept {
            return capacity;
        }

        size_t alloc_size() const noexcept {
            return size;
        }

        bool is_empty() const noexcept {
            return capacity == 0;
        }

        void erase() noexcept {
            for (size_t i = 0; i < capacity; ++i)
                builder::destroy(a, &storage[i]);
            builder::deallocate(a, storage, size);
            capacity = 0;
            size = 0;
        }


        ~dyn_array() noexcept {
            erase();
        }

        dyn_array() noexcept {
            storage = nullptr;
            size = 0;
            capacity = 0;
        }

        dyn_array(const std::initializer_list<T> &list) : dyn_array() {
            if constexpr (std::is_move_constructible<T>::value) {
                storage = builder::allocate(a, list.size());
                for (const auto &x: list) {
                    builder::construct(a, &storage[capacity], std::move(x));
                    capacity++;
                }
            } else if constexpr (std::is_copy_constructible<T>::value) {
                try {
                    storage = builder::allocate(a, list.size());
                    for (const auto &x: list) {
                        builder::construct(a, &storage[capacity], x);
                        capacity++;
                    }
                } catch (const std::runtime_error &e) {
                    for (size_t i = 0; i < capacity; ++i) {
                        builder::destroy(a, &storage[i]);
                    }
                    builder::deallocate(a, storage);
                }
            } else {
                throw std::runtime_error("can't use initializer list if objects are non-movable and non-copyable");
            }
            size = list.size();
            capacity = size;
        }

        explicit dyn_array(size_t size) : size(size), capacity(0) {
            storage = builder::allocate(a, size);
        }

        dyn_array(const dyn_array &other) {
            if (other.size == 0) {
                size = 0;
                capacity = 0;
                storage = nullptr;
            } else if (other.capacity == 0) {
                storage = builder::allocate(a, other.size);
                size = other.size;
            } else if constexpr (std::is_copy_constructible<T>::value) {
                auto tmp = builder::allocate(a, other.size);
                capacity = 0;
                try {
                    for (; capacity < other.capacity; ++capacity) {
                        builder::construct(a, &tmp[capacity], other.storage[capacity]);
                    }
                } catch (const std::runtime_error &e) {
                    for (size_t i = 0; i < capacity; ++i) {
                        builder::destroy(a, &tmp[i]);
                    }
                    builder::deallocate(a, tmp, other.size);
                    throw;
                }
                storage = tmp;
                size = other.size;
            } else {
                throw std::runtime_error("can't copy array with non-copyable elements");
            }
        }

        dyn_array(dyn_array &&other) noexcept {
            storage = other.storage;
            other.storage = nullptr;
            size = other.size;
            capacity = other.capacity;
            other.size = 0;
            other.capacity = 0;
        }

        dyn_array(gen_iterator<true> begin, gen_iterator<true> end) {
            if constexpr (std::is_copy_constructible<T>::value) {
                size = end - begin;
                storage = builder::allocate(a, size);
                capacity = 0;
                try {
                    for (; begin != end; ++begin, ++capacity) {
                        builder::construct(a, &storage[capacity], *begin);
                    }
                } catch (std::runtime_error &e) {
                    for (size_t i = 0; i < capacity; i++) {
                        builder::destroy(a, &storage[i]);
                    }
                    builder::deallocate(a, storage, size);
                    size = 0;
                }
            } else {
                throw std::runtime_error("Can't construct a copy of an array with non-copyable values");
            }
        }

        const T &operator[](int idx) const {
            return storage[idx];
        }

        T &operator[](int idx) {
            return storage[idx];
        }

        gen_iterator<true> begin() const noexcept {
            return gen_iterator<true>(storage);
        }

        gen_iterator<true> end() const noexcept {
            return gen_iterator<true>(storage + capacity);
        }

        gen_iterator<false> begin() noexcept {
            return gen_iterator<false>(storage);
        }

        gen_iterator<false> end() noexcept {
            return gen_iterator<false>(storage + capacity);
        }

        typedef gen_iterator<true> const_iterator;

        typedef gen_iterator<false> iterator;

        dyn_array &operator=(const dyn_array &other) {
            auto tmp(other);
            std::swap(*this, tmp);
            return *this;
        }

        dyn_array &operator=(dyn_array &&other) noexcept {
            std::swap(storage, other.storage);
            std::swap(capacity, other.capacity);
            std::swap(size, other.size);
            return *this;
        }

        bool operator==(const dyn_array &other) const noexcept(is_comparable<T>::value) {
            if constexpr (is_comparable<T>::value) {
                if (other.capacity != capacity) {
                    return false;
                }
                for (size_t i = 0; i < capacity; ++i) {
                    if (storage[i] != other[i]) {
                        return false;
                    }
                }
                return true;
            } else {
                throw std::runtime_error("Can't compare arrays of non-comparable types");
            }
        }

        bool operator!=(const dyn_array &other) const noexcept(is_comparable<T>::value) {
            return not((*this) == other);
        }
    };


}

#endif //HAT_DYN_ARR_HPP
